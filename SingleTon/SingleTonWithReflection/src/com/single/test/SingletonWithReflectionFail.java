package com.single.test;

import java.io.Serializable;

public class SingletonWithReflectionFail implements Serializable {
	private static final long serialVersionUID = 3119105548371608200L;
	private static final SingletonWithReflectionFail singleton = new SingletonWithReflectionFail();

	private SingletonWithReflectionFail() {

	}

	public static SingletonWithReflectionFail getInstance() {
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Singleton object can't be clone");
	}

	protected Object readResolve() {
		return singleton;
	}

	public static void main(String[] args) {

		try {
			Class<SingletonWithReflectionFail> singletonClass = (Class<SingletonWithReflectionFail>) Class
					.forName("com.single.test.SingletonWithReflectionFail");
			SingletonWithReflectionFail singletonReflectionOne = singletonClass.newInstance();
			System.out.println("singletonReflectionOne:::" + singletonReflectionOne.hashCode());
			SingletonWithReflectionFail singletonReflectionTwo = singletonClass.newInstance();
			System.out.println("singletonReflectionTwo:::" + singletonReflectionTwo.hashCode());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

}
