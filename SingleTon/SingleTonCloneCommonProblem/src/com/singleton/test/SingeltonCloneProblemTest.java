package com.singleton.test;

public class SingeltonCloneProblemTest implements Cloneable {

	private static SingeltonCloneProblemTest instance = null;

	private SingeltonCloneProblemTest() {
		System.out.println("Ankamma pallapu");
	}

	public static SingeltonCloneProblemTest getInstance() {

		if (instance == null) {
			instance = new SingeltonCloneProblemTest();
			return instance;
		}
		return instance;
	}

	public static void main(String[] args) throws CloneNotSupportedException {

		SingeltonCloneProblemTest test1 = SingeltonCloneProblemTest.getInstance();
		System.out.println("test1::::" + test1.hashCode());

		SingeltonCloneProblemTest test2 = (SingeltonCloneProblemTest) test1.clone();
		System.out.println("test2::::" + test2.hashCode());

	}

}
